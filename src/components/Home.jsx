import React from 'react'
import { Content, Footer, Header } from './layout'

export default function Home() {
  return (
      <>
    <Header/>
    <Content/>
    <Footer/>
      
      </>
        
  )
}
