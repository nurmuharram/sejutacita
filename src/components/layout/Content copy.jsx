import { CDataTable } from '@coreui/react'
import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { Table } from 'react-bootstrap'

function VoucherOneHistoryList({ itemid }) {

    const [userData, setUserData] = useState({
        id: '',
        user_name: '',
        claimed_date: ''
    })

    useEffect(() => {
        const config = {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('auth'),
            }
        }
        axios.get(`api/voucher/getVoucherOneUser?id=${itemid}`, config).then(res => {
            const items = res.data;
            setUserData(items);
        });
    }, [itemid])

    const datauser = [
        { key: 'id', _style: { width: '5%' } },
        { key: 'user_name' },
        { key: 'claimed_date' },

    ];

    const date = new Date((userData.claimed_date && userData.claimed_date.split(' ').join('T')) + '.000Z')

    return (
        <div>
            <Table>
                <tr>
                    <th>Id</th>
                    <th>User Name</th>
                    <th>Claimed Date</th>
                </tr>
                <tr>
                    <td>{userData.id}</td>
                    <td>{userData.user_name}</td>
                    <td width='50%'>{date.toString()}</td>
                </tr>
            </Table>
        </div>
    )
}

export default VoucherOneHistoryList
