import React, { useEffect, useState } from 'react'
import axios from 'axios'
import ReactPaginate from 'react-paginate';
export default function Content() {

  const [category, setCategory] = useState([])
  const [bookName, setBookName] = useState([])
  
  const [searchCategory, setSearchCategory] = useState('')

  const [categorypageNumber, setCategorypageNumber] = useState(0)
  const categoryPerPage = 2;
  const categorypagesVisited = categorypageNumber * categoryPerPage;

  const pageCount = Math.ceil(category.length / categoryPerPage);
  const changePage = ({ selected }) => {
      setCategorypageNumber(selected);
  }



useEffect(() => {
    const config = {
        headers: {
            Authorization: 'Bearer ' + localStorage.getItem('auth'),
        }
    }
    axios.get(`/fee-assessment-categories`, config).then(res => {
        const items = res.data;
        setCategory(items);
    });
}, [])


useEffect(() => {
  const config = {
      headers: {
          Authorization: 'Bearer ' + localStorage.getItem('auth'),
      }
  }
  axios.get(`/fee-assessment-books`, config).then(res => {
      const items = res.data;
      setBookName(items);
  });
}, [])

const categoryMapping = category
.slice(categorypagesVisited, categorypagesVisited + categoryPerPage)
.filter((result) => { 


  if (searchCategory === '') {
    return result
} else if (result.name.toLowerCase().includes(searchCategory.toLowerCase())) {
    return result
} 


})
    .map((item, index)=>{
        return(
          <tr>
          <td className='md' align='center'>{item.id}</td>
          <td align='left'>{item.name}</td>
      </tr>  
        )})

console.log(searchCategory)
  

return (
    <div>
        <div className="container">

        <div>
        <table class="table">
        <thead>
          <input placeholder='Search Category..' onChange={(e)=>setSearchCategory(e.target.value)} />
                <tr>
                    <th>Id</th>
                    <th>Categories</th>
                </tr>
        </thead>

        <tbody>
                {categoryMapping}
        </tbody>         
        </table>
        <ReactPaginate
                                    previousLabel={"Prev"}
                                    nextLabel={"Next"}
                                    pageCount={pageCount}
                                    onPageChange={changePage}
                                    containerClassName={"paginationBttns"}
                                    previousLinkClassName={"PreviousBttn"}
                                    nextLinkClassName={"nextBttn"}
                                    disabledClassName={"paginationDisabled"}
                                    activeClassName={"paginationActive"}
                                />

        </div>

        <div>
        <table class="table">
        <thead>
                <tr>
                    <th>Id</th>
                    <th align='left'>Book Name</th>
                </tr>
        </thead>

        <tbody>
                {bookName.map((item, index)=>{
                  return(
                    <tr>
                    <td>{item.id}</td>
                    <td align='left'>{item.name}</td>
                </tr>  
                  )
                })}
        </tbody>         
        </table>
        </div>


        </div>          
    </div>
  )
}
